from distutils.command.build import build
from distutils.core import setup

import os
import sys

class build_with_mo (build):
    """Build package files, including message catalogs for Django."""
    def run (self):
        from django.core.management import call_command
        cwd = os.path.dirname(os.path.abspath(__file__))
        os.chdir(os.path.join(cwd,'seminar'))
        call_command('compilemessages')
        os.chdir(cwd)
        build.run(self)

setup(
    name='django-seminar',
    version='0.1',
    author='Emmanuel Beffara',
    author_email='manu@beffara.org',
    packages=['seminar', 'seminar.templatetags'],
    package_data={
        'seminar': [
            'locale/*/*/*.mo',
            'locale/*/*/*.po',
            'static/css/*.css',
            'templates/seminar/*.html',
        ]},
    license='BSD',
    description='Handles conference-like events with talks and schedules.',
    cmdclass={'build': build_with_mo},
)
