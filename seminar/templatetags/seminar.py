from django import template
from django.template.defaultfilters import date as _date
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import pgettext

register = template.Library()

@register.filter
def name_reference (speaker):
    text = escape(speaker.display_name)
    if speaker.web_page:
        text = '<a href="%s" class="name">%s</a>' % (speaker.web_page, text)
    else:
        text = '<span class="name">%s</span>' % text
    if speaker.institution:
        text = "%s (%s)" % (text, escape(speaker.institution))
    return mark_safe(text)

def my_date_format (date, fmt):
    text = ""
    while fmt != "":
        pos_open = fmt.find("{")
        if pos_open < 0:
            text += fmt
            break
        pos_end = fmt.find("}", pos_open)
        if pos_end < 0:
            pos_end = len(fmt)
        text += fmt[:pos_open] + _date(date, fmt[pos_open+1:pos_end])
        fmt = fmt[pos_end+1:]
    return text


@register.filter
def date_range (begin, end):
    if begin.year != end.year:
        fmt_begin = pgettext("date range spanning several years", "{F jS, Y}")
        fmt_end = pgettext("date range spanning several years", " to {F jS, Y}")
    elif begin.month != end.month:
        fmt_begin = pgettext("date range spanning several months", "{F jS}")
        fmt_end = pgettext("date range spanning several months", " to {F jS, Y}")
    elif begin.day != end.day:
        fmt_begin = pgettext("date range in a single month", "{F jS}")
        fmt_end = pgettext("date range in a single month", " to {jS, Y}")
    else:
        fmt_begin = pgettext("single-day date range", "{F jS, Y}")
        fmt_end = ""
    return my_date_format(begin, fmt_begin) + my_date_format(end, fmt_end)
