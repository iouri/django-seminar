# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seminar', '0003_auto_20160706_0131'),
    ]

    operations = [
        migrations.AddField(
            model_name='talk',
            name='rank',
            field=models.IntegerField(default=0, help_text='The position in the list of talks within a session (otherwise, alphabetical order is applied)'),
        ),
    ]
