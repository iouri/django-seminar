# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seminar', '0002_auto_20160705_1242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='chair',
            field=models.CharField(help_text='The name of the session chair, if any', max_length=120, null=True, verbose_name='chair name', blank=True),
        ),
    ]
