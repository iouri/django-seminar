# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seminar', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScheduledTalk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='session',
            name='talk',
        ),
        migrations.AddField(
            model_name='session',
            name='chair',
            field=models.CharField(max_length=120, null=True, verbose_name='chair name', blank=True),
        ),
        migrations.AddField(
            model_name='scheduledtalk',
            name='session',
            field=models.ForeignKey(verbose_name='session', to='seminar.Session', help_text='The session'),
        ),
        migrations.AddField(
            model_name='scheduledtalk',
            name='talk',
            field=models.OneToOneField(verbose_name='talk', to='seminar.Talk', help_text='The talk for this session'),
        ),
    ]
