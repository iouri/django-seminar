# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seminar', '0004_talk_rank'),
    ]

    operations = [
        migrations.AddField(
            model_name='talkkind',
            name='plural',
            field=models.CharField(help_text="Plural of verbose name (by default, a final 's' is added)", max_length=80, null=True, verbose_name='plural name', blank=True),
        ),
    ]
