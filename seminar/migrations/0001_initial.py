# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import seminar.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='The name of the attachment', max_length=80)),
                ('file', models.FileField(help_text='A file to upload', upload_to=seminar.models.attachment_upload_path, blank=True)),
                ('url', models.URLField(help_text='The URL of the attachment', blank=True)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='An optional title for the event', max_length=40, verbose_name='title', blank=True)),
                ('slug', models.SlugField(help_text='A symbolic name for use in URLs (a default value is computed from the title and date)', unique=True, verbose_name='slug')),
                ('date_begin', models.DateField(help_text='The date of the first day of the event', verbose_name='first day')),
                ('date_end', models.DateField(help_text='The date of the last day of the event', verbose_name='last day')),
                ('short_description', models.TextField(help_text='A short presentation, in Markdown format', verbose_name='short description', blank=True)),
                ('long_description', models.TextField(help_text="Details to put on the event's page, in Markdown format", verbose_name='long description', blank=True)),
                ('post_scriptum', models.TextField(help_text='Comments to insert after the programme, in Markdown format', verbose_name='post-scriptum', blank=True)),
            ],
            options={
                'ordering': ['-date_begin'],
            },
        ),
        migrations.CreateModel(
            name='EventKind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(help_text='A symbolic name, for use e.g. in CSS', verbose_name='slug')),
                ('name', models.CharField(help_text='A verbose name for display', max_length=80, verbose_name='name')),
                ('template_event_base', models.CharField(default=b'seminar/event_base.html', help_text='A base template for event pages of this kind', max_length=100, verbose_name='base template')),
                ('template_session_base', models.CharField(default=b'seminar/session_base.html', help_text='A base template for session pages of this kind', max_length=100, verbose_name='base template')),
                ('template_talk_base', models.CharField(default=b'seminar/talk_base.html', help_text='A base template for talk pages of this kind', max_length=100, verbose_name='base template')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(help_text='The day when this item occurs', verbose_name='date')),
                ('time_begin', models.TimeField(help_text='The scheduled time of beginning', verbose_name='starting time')),
                ('time_end', models.TimeField(help_text='The scheduled time of ending', verbose_name='ending time')),
                ('title', models.CharField(help_text='A short description, or a subtitle for a talk', max_length=120, null=True, verbose_name='title', blank=True)),
                ('event', models.ForeignKey(editable=False, to='seminar.Event', help_text='The event this session is part of')),
            ],
            options={
                'ordering': ('date', 'time_begin'),
            },
        ),
        migrations.CreateModel(
            name='Speaker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rank', models.IntegerField(help_text='The position in the list of speakers', editable=False)),
                ('name', models.CharField(max_length=120, null=True, verbose_name='name', blank=True)),
                ('web_page', models.URLField(verbose_name='web page', blank=True)),
                ('institution', models.CharField(max_length=120, null=True, verbose_name='institution', blank=True)),
            ],
            options={
                'ordering': ('talk', 'rank', 'name'),
            },
        ),
        migrations.CreateModel(
            name='Talk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='A title or a short description', max_length=200, null=True, verbose_name='title', blank=True)),
                ('slug', models.SlugField(help_text="A symbolic name for use in URLs (a default value is computed from the speakers' names)", verbose_name='slug')),
                ('abstract', models.TextField(help_text='An abstract or long description, in Markdown format', null=True, verbose_name='abstract', blank=True)),
                ('featured', models.BooleanField(default=True, help_text='Whether this talk appears in the short announcement', verbose_name='featured')),
                ('name_list', models.CharField(default=b'', help_text='The list of speaker names, automatically computed', max_length=120, editable=False)),
                ('event', models.ForeignKey(to='seminar.Event')),
            ],
            options={
                'ordering': ('name_list', 'title'),
            },
        ),
        migrations.CreateModel(
            name='TalkKind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(help_text='A symbolic name, for use e.g. in CSS', verbose_name='slug')),
                ('name', models.CharField(help_text='A verbose name for display', max_length=80, verbose_name='name')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='talk',
            name='kind',
            field=models.ForeignKey(verbose_name='kind', to='seminar.TalkKind', help_text='The kind of item in the schedule'),
        ),
        migrations.AddField(
            model_name='speaker',
            name='talk',
            field=models.ForeignKey(related_name='speakers', to='seminar.Talk', help_text='The talk this speaker is defined for'),
        ),
        migrations.AddField(
            model_name='session',
            name='talk',
            field=models.ForeignKey(blank=True, to='seminar.Talk', help_text='The talk for this session, if any', null=True, verbose_name='talk'),
        ),
        migrations.AddField(
            model_name='event',
            name='kind',
            field=models.ForeignKey(verbose_name='kind', to='seminar.EventKind', help_text='The kind of event'),
        ),
        migrations.AddField(
            model_name='attachment',
            name='talk',
            field=models.ForeignKey(to='seminar.Talk'),
        ),
        migrations.AlterUniqueTogether(
            name='talk',
            unique_together=set([('event', 'slug')]),
        ),
    ]
