# coding: UTF-8

import datetime
import string

from django.contrib.auth.models import *
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db.models import *
from django.template.defaultfilters import slugify
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

class SlugNKManager (Manager):
    """
    This is a manager that uses a model's slug field as a natural key.
    """
    def get_by_natural_key (self, slug):
        return self.get(slug=slug)

class SlugNKModel (Model):
    """
    This abstract class extends the base Model class using its slug field as a
    natural key.
    """
    class Meta:
        abstract = True

    objects = SlugNKManager()

    def natural_key (self):
        return (self.slug,)

@python_2_unicode_compatible
class EventKind (SlugNKModel):
    """
    Represents a kind of event, for presentation purposes.
    """
    slug = SlugField(verbose_name = _("slug"),
        help_text = _("A symbolic name, for use e.g. in CSS"))
    name = CharField(verbose_name = _("name"), max_length = 80,
        help_text = _("A verbose name for display"))
    template_event_base = CharField(verbose_name = _("base template"),
        max_length=100, default = 'seminar/event_base.html',
        help_text = _("A base template for event pages of this kind"))
    template_session_base = CharField(verbose_name = _("base template"),
        max_length=100, default = 'seminar/session_base.html',
        help_text = _("A base template for session pages of this kind"))
    template_talk_base = CharField(verbose_name = _("base template"),
        max_length=100, default = 'seminar/talk_base.html',
        help_text = _("A base template for talk pages of this kind"))

    def __str__ (self):
        return self.name
    objects = SlugNKManager()

@python_2_unicode_compatible
class Event (SlugNKModel):
    """
    Represents an event.
    """
    kind = ForeignKey(EventKind, verbose_name = _("kind"),
        help_text = _("The kind of event"))
    title = CharField(verbose_name = _("title"), max_length = 40, blank = True,
        help_text = _("An optional title for the event"))
    slug = SlugField(verbose_name = _("slug"), unique = True,
        help_text = _("A symbolic name for use in URLs"
            " (a default value is computed from the title and date)"))
    date_begin = DateField(verbose_name = _("first day"),
        help_text = _("The date of the first day of the event"))
    date_end = DateField(verbose_name = _("last day"),
        help_text = _("The date of the last day of the event"));
    short_description = TextField(verbose_name = _("short description"), blank = True,
        help_text = _("A short presentation, in Markdown format"))
    long_description = TextField(verbose_name = _("long description"), blank = True,
        help_text = _("Details to put on the event's page, in Markdown format"))
    post_scriptum = TextField(verbose_name = _("post-scriptum"), blank = True,
        help_text = _("Comments to insert after the programme, in Markdown format"))

    class Meta:
        ordering = ['-date_begin']

    @property
    def name (self):
        if self.title:
            return self.title
        return self.kind.name

    def __str__ (self):
        return '%s, %s' % (self.name, self.date_begin)

    @permalink
    def get_absolute_url (self):
        return ('seminar-event-detail', [str(self.slug)])

    def save (self, *args, **kwargs):
        if not self.slug:
            self.slug = find_unused_slug(
                self.kind.slug + '-' + self.date_begin.strftime('%Y-%m-%d'),
                Event.objects.all())
        super(Event, self).save(*args, **kwargs)

    def day_list (self):
        """
        Return the list of pairs (day number, date) for each day contained in
        the week. The day number starts at 0.
        """
        first = self.date_begin.toordinal()
        last = self.date_end.toordinal()
        return [
            (day - first, datetime.date.fromordinal(day))
            for day in range(first, last+1) ]

    def talks_by_kind (self):
        return self.talk_set.order_by(*(('kind',) + Talk._meta.ordering))

    def featured_talks (self):
        return self.talk_set.filter(featured=True)

    def schedule_is_set (self):
        talks = self.talk_set.annotate(num_sessions=Count('session'))
        return not talks.filter(num_sessions=0).exists()

def find_unused_slug (base, queryset):
    """
    Given a base slug, return a slug that is unused in the given queryset's
    objects' 'slug' fields, by appending a dash and a number if necessary.
    """
    slugs = [obj.slug for obj in queryset.filter(slug__startswith=base)]
    slug = base
    count = 0
    while slug in slugs:
        count += 1
        slug = '%s-%d' % (base, count)
    return slug

@python_2_unicode_compatible
class TalkKind (SlugNKModel):
    """
    Represents a kind of talk, for presentation purposes.
    """
    slug = SlugField(verbose_name = _("slug"),
        help_text = _("A symbolic name, for use e.g. in CSS"))
    name = CharField(verbose_name = _("name"), max_length = 80,
        help_text = _("A verbose name for display"))
    plural = CharField(verbose_name = _("plural name"), max_length = 80,
        null = True, blank = True,
        help_text = _("Plural of verbose name (by default, a final 's' is added)"))

    def get_plural_name (self):
        return self.plural or self.name + u's'

    def __str__ (self):
        return self.name

class TalkManager (Manager):
    """
    This manager use a pair of an event slug and a talk slug as natural key.
    """
    def get_by_natural_key (self, event_slug, talk_slug):
        return self.get(event__slug=event_slug, slug=talk_slug)

@python_2_unicode_compatible
class Talk (Model):
    """
    Represents a talk, inside an event.
    """
    event = ForeignKey(Event, editable = True)
    kind = ForeignKey(TalkKind, verbose_name = _("kind"),
            help_text = _("The kind of item in the schedule"))
    title = CharField(verbose_name = _("title"),
            max_length = 200, blank = True, null = True,
            help_text = _("A title or a short description"))
    slug = SlugField(verbose_name = _("slug"),
            help_text = _("A symbolic name for use in URLs"
                " (a default value is computed from the speakers' names)"))
    abstract = TextField(verbose_name = _("abstract"), null = True, blank = True,
            help_text = _("An abstract or long description, in Markdown format"))
    featured = BooleanField(verbose_name = _("featured"), default = True,
            help_text = _("Whether this talk appears in the short announcement"))
    rank = IntegerField(default = 0,
            help_text = _("The position in the list of talks within a session (otherwise, alphabetical order is applied)"))

    name_list = CharField(max_length = 120, default = "", editable = False,
            help_text = _("The list of speaker names, automatically computed"))

    class Meta:
        ordering = ('name_list', 'title')
        unique_together = (('event', 'slug'),)

    objects = TalkManager()

    def natural_key (self):
        return (self.event.slug, self.slug)

    @property
    def name (self):
        if self.speakers is not None:
            text = string.join([s.display_name for s in self.speakers.all()], ", ")
        else:
            text = ""
        if self.title:
            if len(text) != 0:
                text += " - "
            text += self.title
        if len(text) == 0:
            text = self.kind.name
        return text

    def __str__ (self):
        return self.name

    def save (self, *args, **kwargs):
        if not self.slug:
            self.slug = find_unused_slug(
                string.join([ slugify(s.name.split(', ')[0])
                    for s in self.speakers.all() ], '-'),
                Talk.objects.filter(event=self.event))
        self.name_list = string.join([s.name for s in self.speakers.all()], '; ')
        super(Talk, self).save(*args, **kwargs)

    @permalink
    def get_absolute_url (self):
        return ('seminar-talk-detail', (self.event.slug, self.slug))

@python_2_unicode_compatible
class Speaker (Model):
    """
    Represents a speaker for a talk, with a name, a web page, etc.
    """
    talk = ForeignKey(Talk, related_name='speakers',
            help_text = _("The talk this speaker is defined for"))
    rank = IntegerField(editable = False,
            help_text = _("The position in the list of speakers"))
    name = CharField(verbose_name = _("name"),
            max_length = 120, blank = True, null = True)
    web_page = URLField(verbose_name = _("web page"),
            blank = True)
    institution = CharField(verbose_name = _("institution"),
            max_length = 120, blank = True, null = True)

    class Meta:
        ordering = ('talk', 'rank', 'name')

    @property
    def display_name (self):
        if not self.name:
            return ""
        parts = self.name.split(", ")
        if len(parts) == 2:
            parts.reverse()
        elif len(parts) >= 3:
            parts = [parts[2], parts[0], parts[1]]
        return string.join(parts)

    @property
    def short_name (self):
        if not self.name:
            return ""
        return self.name.split(", ")[0]

    def __str__ (self):
        return self.name

@python_2_unicode_compatible
class Session (Model):
    """
    Represents a session in a schedule
    """
    event = ForeignKey(Event, editable = False,
            help_text = _("The event this session is part of"))
    date = DateField(verbose_name = _("date"),
            help_text = _("The day when this item occurs"))
    time_begin = TimeField(verbose_name = _("starting time"),
            help_text = _("The scheduled time of beginning"))
    time_end = TimeField(verbose_name = _("ending time"),
            help_text = _("The scheduled time of ending"))
    chair = CharField(verbose_name = _("chair name"),
            max_length = 120, blank = True, null = True,
            help_text = _("The name of the session chair, if any"))
    title = CharField(verbose_name = _("title"),
            max_length = 120, blank = True, null = True,
            help_text = _("A short description, or a subtitle for a talk"))

    class Meta:
        ordering = ('date', 'time_begin')

    @property
    def name (self):
        if self.title:
            if self.chair:
                return "%s - %s" % (self.title,self.chair)
            return self.title
        sts = self.scheduledtalk_set.all()
        if sts.count() == 1:
            return sts[0].talk.kind.name
        if sts.count() > 1:
            kinds = sorted(sts.values_list('talk__kind',flat=True).distinct())
            if len(kinds) == 1:
                return TalkKind.objects.get(id=kinds[0]).get_plural_name()
            else:
			    return _("Talks")
        if self.event:
            return self.event.name
        return _("Session")

    @property
    def datetime_begin (self):
        return datetime.datetime.combine(self.date, self.time_begin)

    @property
    def datetime_end (self):
        return datetime.datetime.combine(self.date, self.time_end)

    def __str__ (self):
        return "%s, %s, %s" % (self.name, self.date, self.times())

    def talks (self):
        return Talk.objects.filter(scheduledtalk__session=self).order_by('rank','name_list','title')

    def kinds (self):
        kind_pks = self.talks().values_list('kind',flat=True).distinct()
        return TalkKind.objects.filter(pk__in=kind_pks)

    def times (self):
        """
        Returns a string that contains the start and end times of the session.
        """
        return '%d:%02d - %d:%02d' % (
            self.time_begin.hour, self.time_begin.minute,
            self.time_end.hour, self.time_end.minute)

    @permalink
    def get_absolute_url (self):
        return ('seminar-session-detail', (self.event.slug, self.id))

@python_2_unicode_compatible
class ScheduledTalk (Model):
    """
    Associates a talk with a session
    """
    session = ForeignKey(Session, verbose_name = _("session"), help_text = _("The session"))
    talk = OneToOneField(Talk, verbose_name = _("talk"), help_text = _("The talk for this session"))

    def __str__ (self):
        return "%s, %s" % (self.session, self.talk)

def attachment_upload_path (instance, filename):
    return 'talk%d/%s' % (instance.talk.id, filename)

class Attachment (Model):
    """
    A file or an URL attached to a talk.
    """
    talk = ForeignKey(Talk, editable = True)
    name = CharField(max_length = 80,
            help_text = _("The name of the attachment"))
    file = FileField(upload_to = attachment_upload_path, blank = True,
            help_text = _("A file to upload"))
    url = URLField(blank = True,
            help_text = _("The URL of the attachment"))

    class Meta:
        ordering = ('name',)

    def clean (self):
        return
        if (self.file and self.url) or not (self.file or self.url):
            raise ValidationError("Either a file or an URL is required.")

    def get_url (self):
        if self.file:
            return self.file.url
        else:
            return self.url
