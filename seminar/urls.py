from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import permission_required

from seminar.views import *

event_patterns = patterns('seminar.views',
    url(r'^$',
        EventDetailView.as_view(),
        name='seminar-event-detail'),
    url(r'^edit/$',
        permission_required('seminar.change_event')(EventUpdateView.as_view()),
        name='seminar-event-edit'),
    url(r'^delete/$',
        permission_required('seminar.delete_event')(EventDeleteView.as_view()),
        name='seminar-event-delete'),

    # talks

    url(r'^talks/$',
        TalkListView.as_view(),
        name='seminar-talk-list'),
    url(r'^talks/new_talk/$',
        permission_required('seminar.add_talk')(TalkCreateView.as_view()),
        name='seminar-talk-create'),
    url(r'^talks/(?P<slug>[-a-z0-9]+)/$',
        TalkDetailView.as_view(),
        name='seminar-talk-detail'),
    url(r'^talks/(?P<slug>[-a-z0-9]+)/edit/$',
        permission_required('seminar.change_talk')(TalkUpdateView.as_view()),
        name='seminar-talk-edit'),
    url(r'^talks/(?P<slug>[-a-z0-9]+)/delete/$',
        permission_required('seminar.delete_talk')(TalkDeleteView.as_view()),
        name='seminar-talk-delete'),

    # sessions

    url(r'^sessions/$',
        SessionListView.as_view(),
        name='seminar-session-list'),
    url(r'^sessions/new_session/$',
        permission_required('seminar.add_session')(SessionCreateView.as_view()),
        name='seminar-session-create'),
    url(r'^sessions/(?P<pk>\d+)/$',
        SessionDetailView.as_view(),
        name='seminar-session-detail'),
    url(r'^sessions/(?P<pk>\d+)/edit/$',
        permission_required('seminar.change_session')(SessionUpdateView.as_view()),
        name='seminar-session-edit'),
    url(r'^sessions/(?P<pk>\d+)/delete/$',
        permission_required('seminar.delete_session')(SessionDeleteView.as_view()),
        name='seminar-session-delete'),

    url(r'^timetable/$',
        EventTimeTableView.as_view(),
        name='seminar-event-timetable'),
)

urlpatterns = patterns('seminar.views',
    url(r'^$',
        EventListView.as_view(),
        name='seminar-event-list'),
    url(r'^archives/$',
        EventListDefaultArchiveView.as_view(),
        name='seminar-event-list-archive'),
    url(r'^archives/(?P<year>\d{4})/$',
        EventListYearArchiveView.as_view(),
        name='seminar-event-list-year-archive'),
    url(r'^new_event/$',
        permission_required('seminar.add_event')(EventCreateView.as_view()),
        name='seminar-event-create'),
    url(r'^sessions.ics$',
        SessionFeed(),
        name='seminar-session-feed'),
    url(r'^(?P<event_slug>[-a-z0-9]+)/', include(event_patterns)),
)
